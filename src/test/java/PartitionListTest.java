import org.junit.Assert;
import org.junit.Test;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.*;

public class PartitionListTest {

    @Test(expected=NullPointerException.class)
    public void should_return_list_of_list_by_chunk_size_3_when_list_is_null() {
        PartitionList partitionList = new PartitionList();
        final List<Integer> numbers = null;
        final int chunkSize = 3;
        final AtomicInteger counter = new AtomicInteger();

        final Collection<List<Integer>> list = partitionList.executePartitionListByChunkSize(numbers, chunkSize, counter);
        assertNull(list);
    }

    @Test
    public void should_return_list_of_list_by_chunk_size_3_when_list_is_not_null() {
        PartitionList partitionList = new PartitionList();
        final List<Integer> numbersForList1 = List.of(1,2,3,4,5);
        final int chunkSize = 3;
        final AtomicInteger counterForList1 = new AtomicInteger();

        final Collection<List<Integer>> list2 = List.of(List.of(1,2,3),List.of(4,5));//[[1, 2, 3], [4, 5]]
        final Collection<List<Integer>> list1 = partitionList.executePartitionListByChunkSize(numbersForList1, chunkSize, counterForList1);

        Assert.assertArrayEquals(list1.toArray(),list2.toArray());
    }

    @Test
    public void should_return_list_of_list_by_chunk_size_2_when_list_is_not_null() {
        PartitionList partitionList = new PartitionList();
        final List<Integer> numbersForList1 = List.of(1,2,3,4,5);
        final int chunkSize = 2;
        final AtomicInteger counterForList1 = new AtomicInteger();
        final Collection<List<Integer>> list2 = List.of(List.of(1,2),List.of(3,4),List.of(5));//[[1, 2], [3, 4], [5]]

        final Collection<List<Integer>> list1 = partitionList.executePartitionListByChunkSize(numbersForList1, chunkSize, counterForList1);
        Assert.assertArrayEquals(list1.toArray(),list2.toArray());
    }

    @Test
    public void should_return_list_of_list_by_chunk_size_1_when_list_is_not_null() {
        PartitionList partitionList = new PartitionList();
        final List<Integer> numbersForList1 = List.of(1,2,3,4,5);
        final int chunkSize = 1;
        final AtomicInteger counterForList1 = new AtomicInteger();

        final Collection<List<Integer>> list2 = List.of(List.of(1),List.of(2),List.of(3),List.of(4),List.of(5));//[[1], [2], [3], [4], [5]]
        final Collection<List<Integer>> list1 = partitionList.executePartitionListByChunkSize(numbersForList1, chunkSize, counterForList1);

        Assert.assertArrayEquals(list1.toArray(),list2.toArray());
    }
}