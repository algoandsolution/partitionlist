import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class PartitionList {
    public Collection<List<Integer>> executePartitionListByChunkSize(final List<Integer> integerList, final int chunkSize, final AtomicInteger counter){
        if(Objects.isNull(integerList) && integerList.size() == 0) {
            throw new NullPointerException("List can't be empty or null!!");
        }
        return integerList.stream()
                .collect(Collectors.groupingBy(it -> counter.getAndIncrement() / chunkSize))
                .values();
    }

}
