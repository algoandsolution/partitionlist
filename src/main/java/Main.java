import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    public static void main(String[] args){
        PartitionList list = new PartitionList();
        final List<Integer> numbers = List.of(1,2,3,4,5);//change for testing
        final int chunkSize = 1;//change for testing
        final AtomicInteger counter = new AtomicInteger();

        final Collection<List<Integer>> partitionList =  list.executePartitionListByChunkSize(numbers, chunkSize, counter);

        System.out.println(partitionList);
    }
}
